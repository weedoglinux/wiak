THE WIAK README

Wiak has been tested on Puppy Linux v.2.171 and 3.01.
The dotpet registers with PETget so that you can easily uninstall it if 
you wish. The dotpet installs the following:

As a general purpose component, wiak is installed in /usr/local/bin. Its
help file, wiakusage.txt, its manual page wiakman.txt, and a copy of
this readme file, wiakreadme.txt, is stored in /usr/local/wiakapps/wiak.
Finally, a mini-icon for program menus called, mini-wiak.xpm, is stored
in /usr/local/wiakapps. No other files are installed with this download.
__________________________________________________________________________

    * What Is WIAK(TM)?

    * Why use WIAK(TM)?

    * The WIAK Programming Philosophy

    * Using WIAK in your own programs

    * Creating a new wiakapp via modification of one of the wiak develop 
      exemplars

    * WIAK Manual (i.e. man page)


* What Is WIAK?

Wiak is a simple, limited, but very efficient, special-purpose, usually 
non-interactive, extremely lightweight IPC tool written in C.

WIAK stands for, the "WIAK Interface f�r Application Kontrolle". As an 
Inter-Process Communications utility, its name reflects its ability to 
create and control diverse wiakapps whose component parts may be written 
in a variety of programming languages and with an assortment of GUI 
toolkits. Actually, I'm kind of joking because reality is that W.I.A.K. 
are the first names of my family (compare with how awk is named).


* Why use WIAK?

For one thing, wiak provides IPC facilities (such as System V message 
queues functionality) to languages that don't have easy, flexible or any 
access to them (such as bash script). It also works well with the likes
of gtk-server or gtkdialog to create event driven GUI or commandline
user interfaces.

It is intended that wiak will also provide UNIX and INET socket 
capability as an addition and alternative to its default named-pipe (fifo) 
communication channel(s) and the System V message queues. Using wiak as an 
interface in a new application thus automatically provides the backend (or 
frontend) application with a convenient communications upgrade path. 
Furthermore, since wiak is written directly in C, it facilitates system 
and process control in ways that are often difficult to achieve in some 
other languages (such as bash). Wiak thus acts as a simple but efficient, 
versatile and consistent communications interface for commandline-driven 
applications written in bash or in any other programming language.

Though wiak itself is written in C, there is no requirement that the other 
parts of a wiakapp need be.

The use of wiak thus liberates the programmer in many ways, simplifies the 
programming task, and helps to future-proof the application's code.

Finally, wiak is also extremely efficient in its use of system resources:

Each time wiak itself is called, it efficiently performs its task (usually 
a "--send command" operation) and immediately thereafter closes down 
(releasing any resources it was momentarily using). 


* The WIAK Programming Philosophy

A major philosophy of the "wiak programming method", in general, is to 
keep the User Interface (UI) part of the wiakapp code separate from the 
main processing. This methodology greatly simplifies the overall coding 
effort and allows the programmer to readily use different programming 
languages and GUI widget sets (or non-graphical dialogs) for the user 
interface.

One of the many advantages of the WAIKapp programming model is that it 
facilitates the creation of different clients for the same backend 
server(s), or indeed, different backend server(s) for the same GUI-client. 
Different versions of any wiakapp can thus be created relatively easily 
for different systems; the GUI-client, for example, being a separate 
component, can be simply re-implemented using alternative coding languages 
and GUI toolkits, and take different design forms, to match the needs of 
the user or the particular OS distribution.


* Using WIAK in your own programs

The easiest way to learn how to use wiak is to first read the short and 
simple WIAK tutorial and then to just to study the code of the example 
wiakapps to see how they were made. For example, take a look at wiakdevelop. 
Please also check the provided manual page for wiak, which explains, in some
detail, how to use wiak from the bash commandline. 


* Creating a new wiakapp via modification of one of the "Wiak Develop" 
  exemplars

The typical wiakapp takes the following form (where UI means User 
Interface):

   UI -> WIAK go-between -> backend server0 -> underlying routines/processes.
      -> WIAK go-between -> backend server1 -> underlying routines/processes.
      -> WIAK go-between -> backend server2 -> underlying routines/processes.
         ...
      -> WIAK go-between -> backend serverN -> underlying routines/processes.

Note that a wiakapp may involve one or more backend servers.

The User Interface client application may, for example, be written in 
dialog or it might be a GUI written with perhaps gtk-server gtkdialog 
in bash (or in some other language/widget set).

WIAK(TM) acts as a go-between the UI client and the backend server.

The backend server itself calls up the underlying commandline utilities. 
It is written in any chosen suitable programming language such as 
bash script or C. It can use a different language from any client program.

------------------

WIAK manual:

wiak(1)                    User Commands                          wiak(1)

NAME
    wiak and its tinier children (twiaks): wiakf, wiakmv etc...

SYNOPSIS 

    wiak[mode] [options] [program_name]

    i.e. Depending on which member of the wiak family
         you wish to use, the program name can be 
         either: wiak, wiakf, wiakmv (and so on).
	
NOTES: All of the following, when contextually 
       appropriate, applies to the various twiaks
       as well.

       if [program_name] is used, the wiak
       return value is the program_name pid.
       Otherwise it returns value 0.

DESCRIPTION

    wiak is a specialised but limited non-interactive Inter-Process 
    Communications (IPC) shell written in C, using standard libraries, for 
    portability, high speed, and resource usage efficiency. It does not
    require bash or any other underlying shell, though it works well with
    such shells too.

OPTIONS

-i  --init                 
       To tell wiak to create the fifo.
-q  --quit                 
       To tell wiak to clean up the message box
       (e.g. to delete a named wfifo pipe)
-s  --send <data>          
       Data to send to comms_channel (e.g. fifo). 
       Default is '\n' terminated string of chars.
       Generally, the most used client option.
-r  --receive <location>          
       This receives (i.e. reads) the message box
       contents and puts them to the location
       device (currently can only use stdout 
       as the location device). 
-m  --mode <channel type> 
       IPC mode to use (optional)
       currently either fifo or msgqv
       Default (in wiak) is fifo channel.
-c  --channel <alphanumeric name>
       If no -c value is supplied then the default
       --channel name "wfifo" is used. 
       The IPC channel name is derived from a
       combination of --channel name and the
       numeric zid value.
       In fifo mode it provides the first part
       of the fifo name. If a numeric zid value
       is also provided, it is usually appended.
       Consult the code logic for more info and
       refer to --zid details below.
-z  --zid <integer value>
       In SysV message queue mode:
       this optional parameter is the primary
       determinant of the SysV message queue 
       name key:
       [Note that if no --zid value is supplied 
        then the default --zid value 45678 is used]
       if --zid=-1234567890 (the special case),
        the msgqv name = forked_server process_id.
       (Note that if no server is being started up,
       then forked_server_pid = 0)
       else if -c="" then the --zid value provides 
                        the msgqv key numeric name.
       If -c="X" (where X is somestring) and --zid
       not equal to -123456789 then the msgqv name
       key will be calculated from,
             forked_server pid * 1024 + zid value.
        
       In fifo mode:
       this optional param provides the second 
       part of the overall fifo name used:
       if -z=-1234567890 (the special case), 
       then if -c="X" (where X is somestring) then 
               fifo name = Xforked_server_pid.
       (Note that if no server is being started up,
       then forked_server_pid = 0).
       Based on the above, if -c="" and 
       -z=1234567890 then
               fifo name = forked_server_pid. 
       if -c="somestring" and --zid=N (where N is 
       not equal to -1234567890) then 
       fifo name = somestringN.
       Otherwise, if no -c value is supplied, 
       and --zid=N (where N is not equal to 
       -1234567890) then fifo name used = wfifoN.
 -f  --file <filename>
       RESERVED (provided as cmdline arg 
       to backend server); the programmer can 
       use this in any way they wish in backend
       servers they write
       (as with option --dir).
-d  --dir <dirname>
       RESERVED (provided as cmdline arg 
       to backend server)
-w  --wiaktmp <path>
       Location (i.e. dir path) of wiak fifos 
       (optional; default is WIAKTMP if set,
        else /tmp/wiak/)  
-t  --type <integer n>
       Type of data to send (C-type char string 
       or binary structured).
       The default is C-type char string.
       With SysV message queues, the --type value
       allows messages to be selected by the
       receiver out of the SysV message queue
       in a type dependent manner. In that
       scenario the type value can be used
       as an indicator of message priority.
-l  --length <integer n>
       When sending data, 
       when a negative value is used, its
       absolute size indicates the number of 
       bytes to send out of the output buffer.
       When a positive value is used, the 
       strlen of the output buffer is sent and
       n indicates the character(s) wiak will
       automatically append onto the end of the
       --send data string as follows:
       n = 0 means no extra chars are appended;
       1 = '\n' only; 2 = '\n' then a '\0'; 
       3 = '\0' only (ie. a simple C char string);
       4 = '\t' only; 5 = '\t' then a '\0'; 
       6 = '\t', then a '\n'; 
       7 = '\t', then a '\n' and then a '\0'; 
       default is 1, which results in the same
       behaviour as the default bash echo command
       when that is used with no options.

       When receiving data, 
       when a negative value is used, its
       absolute size indicates the number of 
       bytes to read in from the message box
       (in receive mode this mechanism is 
       generally only useful with IPC fifo mode).
       A positive value gives undefined results.
-p  --perms <octal value>
       Standard UNIX style (user, group, others) 
       rwx permissions for the fifo or 
       SysV message queue.
-h  --help
       Help on usage.
-v  --version
       Software version.

ARGUMENTS

	If an argument remains after option processing,
	that argument is assumed to be the name of a 
	program wiak is to fork and execute (i.e. run).
	
QUOTING

	Quotation marks and other non-alphanumeric 
	characters have no special meaning when sent 
	using wiak's --send option. They are treated
	as normal data and will be remain as part of
	any data string sent.

BUGS

	None known. You should not however use the wiak to 
	start up a copy of itself. Since the same commandline options
	are sent to the program being raised, the result would be an
	exponentially increasing chain reaction of wiak processes
	being birthed. You would have to reboot to clear them.
	
AUTHOR

    William McEwan.
       
REPORTING BUGS

    Report bugs to <wiakapps A_t wiak D_o_t org>.
       
COPYRIGHT

    Copyright (C) 2007 William McEwan, WIAK.ORG.
    This is free software. You may redistribute copies of it under the
    terms of the GNU General Public License (version 3)
    <http://www.gnu.org/licenses/>. There is NO WARRANTY, to the
    extent permitted by law.
       
EXAMPLES OF USING WIAK FROM THE COMMANDLINE

Note: For all wiak IPC modes, you can either use the more bloated but almost
      as efficient wiak itself (which includes the combined code for all IPC
      modes on offer by the wiak family) or you can use the mode-specific 
      tiny wiak. For example, you could use wiak in its default mode for fifo 
      comms (or explicitly specified as wiak --mode fifo) or use the 
      tiny wiak, "wiakf", which does not contain code for any other IPC modes 
      and is therefore slightly faster and more efficient. Wiakf still 
      includes all of the full wiak's fifo handling functionality. If you use 
      a tiny wiak you don't of course need to supply any --mode <IPC mode>
      argument to the command (though you can if you wish; it will just be
      ignored in the twiak anyway). 

      For brief wiak usage information enter: wiak --help (or wiak -h)

The remainder of this usage summary primarily refers to the full version wiak
of wiak (though most of it applies to the twiaks as well).

You can of course try all of the given examples out from a console window 
(or, say, two separate console windows, one for sending and one for 
receiving to stdout). If you use a single console window, you'd need to 
mainly run the commands in the background with & in order to keep on 
typing.


Using wiak with named pipes (i.e. --mode fifo): 

Note that, in practice, you don't have to specify "--mode fifo" because, 
for historical reasons, fifos are used by default anyway. The preferred 
wiak-available alternative mode, "System V IPC message queues" (wiak 
--mode msgqv), are generally more flexible, offer increased functionality, 
and are easier to use than fifos because they don't suffer from most of 
the latter's synchronisation problems). Nevertheless, many Linux/UNIX 
users are more familiar with named pipes so that mode will be discussed 
first in relation to bash.

Some of wiak's named pipe handling functionality can be directly simulated 
with bash commands (though wiak itself uses only a fraction of the 
resources used by bash, and is designed to perform its specialised 
functions more efficiently and usually with enhanced options). For those 
familiar with bash, however, the following list of "equivalents" is useful 
in understanding how to use wiak. These first examples are for named pipes 
only (i.e. fifos), since bash itself (i.e. without wiak's help) has no inbuilt 
functionality for using SysV message queues (which are generally more 
flexible and easier to use and synchronise than named pipes). The list below
first shows the wiak long option version, then its short option version, and 
finally the "equivalent" (but more resource hungry and slower) bash 
version (when such a "equivalent" is available):

Create a named pipe called "mypipe":

 wiak long version:  wiak --channel mypipe --init

  OR, you would most likely use the tinier twiak version specially written to 
  work only for fifos since it is slightly more resource efficient since it
  doesn't need to include code for other IPC modes as well. That is: 
  wiakf --channel mypipe --init 

wiak short version:  wiak -c mypipe -i init
      bash version:  mkfifo /tmp/wiak/mypipe
      
Notes:

a. You can use a different location for the fifo by including the wiak 
option --wiaktmp:
e.g. Create a named pipe in directory /anywhere/anydir:

 wiak long version: wiak --wiaktmp /anywhere/anydir --channel mypipe --init
wiak short version: wiak -w /anywhere/anydir -c mypipe -i

b. You can specify the fifo's (or SysV message queue's) standard UNIX 
permissions (i.e. rwx for users, groups, and others) using, for example: 
wiak --channel mypipe --perms 764 --init (where 764 means in this case 
rwxrw-r--)

[For the following examples, if you haven't already done so, remember to 
create the mypipe fifo first. Note that you CAN use wiak to --send data to 
an ordinary regular file too, not just to a named fifo, but then its 
behavior is different from echo and cat because it writes to the beginning
of the file rather than overwriting it or appending to it (actually it
writes over what already exists at the beginning of the file), and on 
receiving the data only reads up to the first '\0' character encountered. 
This additional functionality is likely to be enhanced in a future wiak 
version.]
      
Send string to named pipe (assuming already done: wiak -c mypipe --init):

 wiak long version:  wiak --channel mypipe --send "hello world"
wiak short version:  wiak -c mypipe -s "hello world"
      bash version:  echo "hello world" > /tmp/wiak/mypipe 
      (OR: echo -e "hello world\n" > /tmp/wiak/mypipe)
      
Note that, if no --length special positive value is provided, wiak by default
will append a newline character (i.e. '\n') to the --send data. This mirrors
the default behaviour of the bash echo command when it is not supplied with
any options. Refer to the main man page --length description for other
options.

Receive string from named pipe (output received on stdout device):

 wiak long version:  wiak --channel mypipe --receive stdout
wiak short version:  wiak -c mypipe -r -
      bash version:  cat < /tmp/wiak/mypipe (or: cat /tmp/wiak/mypipe)
      
Any of the above can be used to fetch the named pipe data into a 
bash variable (for example into bash variable mydata) as follows:

 wiak long version:  mydata=$(wiak --channel mypipe --receive stdout)
wiak short version:  mydata=$(wiak -c mypipe -r -)
      bash version:  mydata=$(cat < /tmp/wiak/mypipe)
or in older bash syntax: mydata=`cat < /tmp/wiak/mypipe`      
      
You can check the above example by then trying: echo $mydata
to see that the data got there (don't forget the $)!
      
You can also use a commandline utility to generate the string to --send to 
a pipe (up to 65 kBytes can be sent in this way with wiak version 2.2.0) and 
automatically read it into wiak's --send option, using bash, for example, 
to do the parsing. The following provides an example of a possibly large 
file listing being sent, but any command or commands which generate output 
on stdout can be used in this manner:

 wiak long version:  wiak --channel mypipe --send "$(ls -al)"
wiak short version:  wiak -c mypipe -s "$(ls -al)"
or, using older bash syntax: wiak -c mypipe -s "`ls -al`" 
which is like bash version:  echo "$(ls -al)" > /tmp/wiak/mypipe
but more simply, for this particular example, bash could use:

                     ls -al > /tmp/wiak/mypipe

Note: you can limit the size of string sent by using wiak's --length 
parameter, including some special cases, as described in the main man page 
--length description.

For example: send only the first 100 characters of the generated string 
"$(ls -al)":

wiak --channel mypipe --send "$(ls -al)" --length -100

I'm not sure, of the top of my head, how to do that with bash alone.


Starting another program in the background from wiak, or bash:

      wiak version:  wiak myprog
      bash version:  myprog &

The above two commands are not directly equivalent, however. The bash 
version leaves bash running, whilst myprog runs in the background. Wiak, 
on the other hand, does not itself consume any resources 
at all after executing myprog into the background (wiak automatically 
terminates, releasing any resources it was using). Note that wiak is an 
independent shell; it does not require bash at all for functionality 
(though it can use any functionality built in to bash (such as in the 
example above  where it utilises the bash inbuilt command ls -al). If you 
do run wiak from bash however, normal bash shell globbing/quoting has to 
be taken into account (and can sometimes be used to advantage).

Wiak provides an additional and alternative way of starting up an external 
program: its --up option. The --up option is used to start a backend 
server program which has usually been specially written for the purpose 
of creating "wiakapps". Wiak passes its own received commandline argument 
string onto the backend server when the --up option is used (in case these 
cmdline args prove useful to the backend server also). Wiak itself 
immediately and automatically thereafter terminates as usual. There isn't 
really a bash equivalent to this that I know of, though you can simply 
start up wiak-compatible backend servers directly from bash (providing the 
backend server with the same options you would have sent it automatically 
via wiak), and then communicate with them via the relevant named pipe 
(bash can't itself, without wiak's help, handle SysV message queues directly
 though). Example of using wiak --up (from the wiakdevelop program):

wiak --up wiakdevserv

You can combine most command options in wiak, so you could, for example 
both create a fifo and raise a backend server using say:  

wiak --channel wfifodevelop --up wiakdevserv

However, one restriction to the above, in the current version of wiak, is 
that you cannot both create a new fifo and --send data in the same wiak 
command.


Using System V message queues instead of fifos

As has been referred to earlier, it is generally actually more convenient 
and useful to use SysV message queues rather than fifos [note that "fifo" was 
the only mode available in earlier versions of wiak than v.2.0.0]. Bash itself 
does not unfortunately provide SysV message queue functionality, and message 
queues can be relatively complex to set up in programming languages such 
as C or Java (etc). However, wiak (version 2.1.0 and above), takes the 
hard work out of that, and presents an interface to System V message queues 
which is almost identical to wiak's fifo interface (but with a few additional 
options provided in order to mirror the extra facilities offered by 
SysV message queues). Wiak also arranges for data received from a SysV message 
queue to be sent to stdout, and such queues thus become available, via wiak, 
to, for example, bash. 

The only major disadvantage of SysV message queues to named pipes
is that the maximum size of each individual message sent to a SysV message 
queue is limited by the underlying system. In the Linux kernel ver. 2.6.21.5 
used during tests, the max message size was 8192 bytes. Wiak will exit with a 
"failed to send data" error message if you try and send more than the max 
message limit to a SysV message queue. Wiak version 2.2.0 also limits the max 
message you can send to a named pipe to 65536 bytes (including any string 
terminating '\0' char); if you try and send more than that to a fifo, wiak 
version 2.2.0 will exit with a harmless "Segmentation fault" error. In 
practice, the max SysV message limit is well in excess of what is required for 
most purposes, so it is generally preferable to use SysV message queues in 
wiakapps rather than use named pipes (you can however use both SysV message queues 
and fifos in the same wiakapp, though each separate wiak call has to be to 
only one or the other at a time).

From the user's point of view, there is little difference between the 
commands used for SysV message queues and the commands used for fifos. The only 
major differences are as follows:

1. Fifos have an alphanumeric name (supplied usually by the wiak --channel 
option); SysV Message Queues have a numeric name (supplied usually by the wiak 
--zid option).

2. You need to suppy the optional argument "--mode msgqv" to wiak to tell 
it to use SysV message queue mode (for named pipes you can but don't need to 
supply the option "--mode fifo" since fifo is the default anyway).
Note, however, that instead of using wiak for SysV message queue
functionality you can alternatively use the "tiny wiak", wiakmv, which
doesn't need any --mode argument to be passed to it since it is a slimmer 
version of wiak designed to work only with SysV message queues (it otherwise
contains the same SysV message queue functionality).

3. The wiak "--length" option probably only has a meaning when sending 
data (but with named pipes it can be used, with "--receive" too, as a kind 
of "fudge" for trying to fix fifo synchronisation problems...).

4. You don't need "--init" to create SysV message queues (they are generated 
automatically as soon as you send a message to a numerically named queue).

5. One of the huge advantages of SysV message queues is that you can send 
messages with differing assigned type numbers, which can then be used by 
the receiving process to extract only certain types of message at a time 
(e.g. a priority system). Unlike with fifos, sending back to back messages 
to SysV message queues thus does not result in one big messed up message 
(messages remain in separate type-based extractable units in the message 
queue). If you don't supply a --type option when using wiak SysV message queue 
mode, the --type value of 1 will be used by default.

With the above comments in mind, the earlier examples all work with 
SysV message queues with an otherwise almost identical syntax. In practice, 
you generally just need to change, for example, 

--channel myfifo 

into: 

--zid <numeric queue name> --mode msgqv --type n

 OR, you would most likely use the tinier twiak version, wiakmv, which has been
 specially written for SysV message queues only and is thus slightly simpler
 to use and slightly more resource efficient since it doesn't include code 
 for other IPC modes as well. It wouldn't need a --mode argument: 
 --zid <numeric queue name> --type n 

(where n here is an integer >= 1)

Note that, instead of needing to use separate fifos for different types of 
data, you can simply now use a single SysV message queue but send data of 
different --type values to it, and then arrange for your receiving program 
to extract and process the messages accordingly to how you want the 
different message types handled.

Here are a few examples repeated from earlier, but note that bash has no 
functional equivalents itself (since bash doesn't have inbuilt message 
queue functionality). Note that bash can however now use wiak to 
send and receive messages to SysV message queues(!) because wiak sends any 
received data to stdout (which makes it accessible to bash variables):

The following sends a C-style string of bytes to what I now generically 
call an IPC mbox.

Fifo mode, wiak long version (using a named pipe called "mypipe"):
  
    wiak --channel mypipe --send "hello world"

SysV message queue mode, wiak long version (using a queue named with key 4567):
  
    wiak --zid 4567 --mode msgqv --type 1 --send "hello world"

 OR: you would most likely use the tinier twiak version specially written to 
 work only for SysV message queues:
    wiakmv --zid 4567 --type 1 --send "hello world"
 (which doesn't require a --mode argument and is slightly more efficient).

or, more simply, and in short form syntax: 

    wiak -z 4567 -m msgqv -s "hello world" 
   [since --type 1 will be used as the message type by default]

(again, in practice you'd probably use wiakmv as an alternative:
    wiakmv -z 4567 -s "hello world")

[Note that no prior wiak --init commandline is needed with SysV message 
queues; i.e. you don't need to first create a queue]

Receive string from an IPC mbox:

Fifo mode, wiak long version:

    wiak --channel mypipe --receive stdout

SysV message queue mode, wiak long version:
  
    wiak --zid 4567 --mode msgqv --receive stdout

or if you want to relay a differently type-classified message than --type 1, 
for example:

mode msgqv wiak long version. Sending:  

    wiak --zid 4567 --mode msgqv --type 2 --send "$(ls -al)"

mode msgqv wiak long version. Receiving the above:  

    wiak --zid 4567 --mode msgqv --type 2 --receive stdout

(you can of course also use a different SysV message queue, if you want to,
as long as the send and receive queues match!)

If, when sending data, you don't want to include the otherwise 
automatically sent newline terminating character '\n', you should supply 
wiak with one of the special --length positive numeric values (refer to the
main man page --length description for details of what will be sent).

e.g. wiak --zid 4567 --mode msgqv --type 2 --send "$(ls -al)" --length 0
does send the whole strlen of the output buffer but doesn't append 
any terminating characters at all to it.

For more general usage hints, study, for example the code for the bash 
scripted wiakdevelop along with that for its bash scripted backend server, 
wiakdevserv.


SEE ALSO

    morfi, wiakrecord, wiakdevelop, wiakdevrec, wiakkillproc, wiakcontrol
    wiakspeak, wiakcmdspk, wiakebookspk
  
wiak 2.3.0                  January 2008	                  wiak(1)
